const getRandomInt = (max: number) => Math.floor(Math.random() * max) + 1;

describe('Демо тесты', () => {
  beforeAll(() => {
    console.log('beforeAll запускается один раз перед всеми тестами!');
  });
  beforeEach(() => {
    console.log('beforeEach запускается перед каждым тестом!');
  });
  afterEach(() => {
    console.log('afterEach запускается после каждого теста!');
  });
  afterAll(() => {
    console.log('after всегда последний!');
  });
  it.skip('Сломанный тест', () => {
    expect(true).toBe(false);
  });

  [1, 2, 3].forEach((value) => {
    it(`Параметризованный тест ${value}`, function () {
      console.log(`Тест ${value}`);
    });
  });

  it.skip('retries', () => {
    jest.retryTimes(2);
    const randomValue = getRandomInt(6);
    const msg = `randomValue = ${randomValue}`;
    console.log(msg);
    expect(randomValue).toBe(6);
  });
});
