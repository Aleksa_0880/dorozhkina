import Client from '../../dev/http-client';

const HttpClient = Client.getInstance();

describe('API группы котов', () => {
  it('Список котов, сгруппированных по группам', async () => {
    const response = await HttpClient.get(`core/cats/allByLetter?limit=1`, {
      responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      groups: expect.arrayContaining([
        expect.objectContaining({
          title: expect.any(String),
          cats: expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              name: expect.any(String),
              description: expect.any(String),
              tags: null,
              gender: expect.any(String),
              likes: expect.any(Number),
              dislikes: expect.any(Number),
            }),
          ]),
          count_in_group: expect.any(Number),
          count_by_letter: expect.any(Number),
        }),
      ]),
      count_output: expect.any(Number),
      count_all: expect.any(Number),
    });
  });
});
