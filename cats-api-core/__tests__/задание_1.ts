import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Самыйлучшийкотвмире', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('API котика', () => {
        beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не удалось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  ///// Задание 1.1
  it.only('Найти существующего котика по id', async () => {
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
        responseType: 'json',
    });
    
    expect(response.body as any).toEqual
    const expectedCat = {
        id: catId,
        ...cats[0],
        likes: 0,
        dislikes: 0,
        tags: null
    };


    expect(response.body as any).toEqual(expect.objectContaining({ cat: expectedCat }));
});

  ///// Задание 1.2
it.skip('Найти котика по некорректному id', async () => {
    await expect(HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
        responseType: 'json',
    })).rejects.toThrowError('Response code 400 (Bad Request)');
});
});
